/**
 * @name        YeetChat-Shared-Functions/index.js
 * @description The main file for YeetChat's shared functions (that are mainly utility-related) for both the client and the server
 * 
 * @version     0.0.1
 * 
 * @author      0J3
 * @author      MorganDGamer
 */

reallog=console.log

gsub = function(s,f,rwith){
    return s.replace(`/${f}/gi`,rwith)
}

// console.log=function(a,b,c){
//     // if (!module.exports.exists(b)) {
//     //     b=""
//     //     if (module.exports.exists(a)) {
//     //         a="Unknown"
//     //         b=a
//     //     } else {
//     //         a="Unknown"
//     //     }
//     // }
//     // if (!module.exports.exists(c)) {
//     //     c=""
//     // }
//     // a,b,c=gsub(a,"\n",`\n[${a}] `),gsub(b,"\n",`\n[${b}] `),gsub(c,"\n",`\n[${c}] `)
//     // reallog(`[${a}] `,b,c)

//     reallog("["+a+"]",b,c)
// }

module.exports = {}
module.exports.exists = function(a) {
    if (typeof(a) == typeof(undefined) || a == typeof(undefined)) {
        return false
    } else {
        return true
    }
}

console.log("Shared Functions","## YeetChat Shared Functions ##\nLoading YeetChat Shared Functions "+`${"v"}${"0.0.1"}`)

module.exports.hasArgument = function(arg){
    for (const a in process.argv) {
        if (process.argv.hasOwnProperty(a)) {
            const element = process.argv[a];
            if (element == arg) {
                return true
            }
        }
    }
    return false
}
module.exports.hasArg=module.exports.hasArgument


module.exports.gsub=gsub
global.gsub=gsub
